import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		System.out.println("Hi user!");
		System.out.println("Type 1 to play Hangman, 2 for Wordle");
		int game = reader.nextInt();
		
		if (game==1){
			//Start Hangman game
			System.out.println("Let's start to play Hangman!");
			System.out.println("Enter a 4-letter word:");
			String word = reader.next().toUpperCase();
			Hangman.runGame(word);
		} else if (game==2) {
			//Start Wordle game
			Wordle.runGame(Wordle.generateWord());
		} else 
			System.out.println("Please only type 1 or 2 to start the game");
	}
}