import java.util.Scanner;
import java.util.Random;
/*
* assignment 3 - 110java - fall semester 2023
* program to create a Wordle game based on Wordle NY Times
* constrained version: only uses words where each letter appears once
*
* user try to guess a 5-letter word 
* user has 6 attempts
*
* letter in word + correct position - coloured in green
* letter in word + incorrect position - coloured in yellow
* letter not in word - coloured in grey
*
* @author Jayden Ly 2333161 - section 4
*/
public class Wordle {
    /*
    * main method
    * generateWord() --> get random word to guess then pass it to runGame()
    * runGame() --> start the game!!
    */
   
    /* 
    * method will pick a word randomly that will be the "target" word
    * chose a word randomly from an array of at least 20 5-letter words
    * words should not have any repeating characters
    * 
    * return the randomly chosen word
    */
    public static String generateWord () {
        Random randGen = new Random();
        // initialise an array of 20 5-letter words
        String[] list = new String[] {"AMBER","BLACK","CRISP","DIVER","FROWN","GLIDE","LODGE","SHARP","SHARK","QUIET","NURSE","RELAX","SWIFT","NUMBER","PRICE","SLIDE","CODER","RUBIK","GLOVE","RELAX"};
        // a random int 0-20
        int x = randGen.nextInt(21);
        return list[x];
    }
    /*
    * method checks if the letter is in the word
    * return true if it is
    * return false if it isn't
    */
    public static boolean letterInWord (String word, char letter) {
        for (int i=0; i<word.length(); i++) {
            if (word.charAt(i) == letter) {
                return true;
            }
        }
        return false;
    }
    /*
    * method checks if the letter: (1) in the word, (2) at the specified position
    * return true if (1) and (2) are met
    * return false if (1) or (2) are unmet
    */
    public static boolean letterInSlot (String word, char letter, int position) {
        for (int i=0; i<word.length(); i++) {
            if (word.charAt(i) == letter && i == position) {
                return true;
            }
        }
        return false;
    }
    /*
    * method will compare answer and guess
    * method will check  whether the letters at each position in the guess were correct or not
    * 
    * return an array of colours where each position in the array 
    * corresponds to a position in the guessed word
    * 
    * Ex: 
    * guess's letter position = answer's letter position --> green
    * 
    * guess letter is in answer
    * but guess's letter position != answer's letter position --> yellow
    * 
    * guess letter is not in answer --> white
    */
    public static String[] guessWord (String answer, String guess) {
        String[] colours = new String [guess.length()];
        char guessedLetter; // letter in the guessed word

        // loop will check if the letter in guess is in the same spot in answer, in answer or not at all
        for (int i=0; i<guess.length(); i++) {
            guessedLetter = guess.charAt(i);
            if (letterInSlot(answer, guessedLetter, i)) {
                colours[i] = "green";
            } else if (letterInWord(answer, guessedLetter)) {
                colours[i] = "yellow";
            } else {
                colours[i] = "white";
            }
        }
        return colours;
    }
    /*
    * method print 5 letters of the guessed word in its corresponding colour in the array
    * print coloured text to the console using "ANSI escape codes"
    * 
    * "\u001B[32m" --> green
    * "\u001B[33m" --> yellow
    * "\u001B[0m"  --> reset the colour back to defaut (white)
    */
    public static void presentResults (String word, String[] colours) {
        // loop will print the color corresponded to the position of the array and apply it to that position of the word
        for (int i=0; i<colours.length; i++) {
            if (colours[i] == "green") {
                System.out.print("\u001B[32m" + word.charAt(i) + " "); // print letter in green
            } else if (colours[i] == "yellow") {
                System.out.print("\u001B[33m" + word.charAt(i) + " "); // print letter in yellow
            } else if (colours[i] == "white") {
                System.out.print("\u001B[0m" + word.charAt(i) + " "); // print letter in white
			}
		}
		System.out.println("\u001B[0m"); // go to next line after print the result + reset color printin
    }
    /*
    * method will ask user to input a guess word
    * ask again if the guess word is not valid (not 5 letters)
    * convert the guess word to uppercase and return
    */
    public static String readGuess () {
        Scanner reader = new Scanner (System.in);
        System.out.println("User guess a word:");
        String guess = reader.next();
        int length = guess.length();

        // while loop will check if the length is exactly 5
        while (length != 5) {
            System.out.println("Invalid guess. Please guess another word:");
            guess = reader.next();
            length = guess.length(); 
        }
        return guess.toUpperCase(); // return the uppercased guessed word
	}
    /* method will run the game
    * call readGuess() to ask the user to input a guess
    * call guessWord() and presentResults() to give user feedback about what letters were in the correct spot
    * 
    * make a loop so user has 6 attempts to guess the correct answer
    * print a congratulatory message if user win
    * print "Try again" if user missed the word and lost after 6 tries
    */
    public static void runGame (String answer) {
        String guess; // contains guessed word from user
        String[] colours; // contains an array of colour corresponded to letter position in guessed word
        int correct = 0; // correct letters in ONE ATTEMPT
        final int maxCorrect = 5; // a word has 5 letter so max of correct is 5

        // for loop give user 6 attempts to guess the answer
        int attempts;
        for (attempts = 6; attempts>0; attempts--) {
            guess = readGuess(); 
            colours = guessWord(answer,guess);
            presentResults(guess,colours);
            correct = 0; // reset correct to 0 after every attempts
            // nested for loop will check if all letters are coloured green --> print congratulatory message + end game
            for (int i=0; i<colours.length; i++) {
                if (colours[i] == "green") {
                    correct++;
                }
            }
            // if statement will check if user guesses the word correctly --> end game
            if (correct == maxCorrect) {
                attempts = 0; // change border condition and end for loop
            }
        }

        // if statement will check if user finally guess the correct answer after attempts 
        if (correct == maxCorrect) {
            System.out.println("Congratulation! You guessed the our secret word. Impressive!");
        } else {
            System.out.println("You missed the our secret word. Try again! Our word is " + answer);
        }
    }
}